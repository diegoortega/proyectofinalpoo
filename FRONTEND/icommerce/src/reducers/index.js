import {AUTH_REQ,AUTH_SUCCESS,AUTH_FAILURE} from '../redux/types';

const initialState = {
  palabra: '',
  datos: [],
  datosfiltrados:[],
  subconjunto:[],
  unarti:{},
  user:{},
  error:'',
  loading:false,
  nombreUsuario:'',
  idEditarArticulo:0,
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ELEJIR":
      return {
        ...state,
        palabra:action.payload, 
      };
    case "NOMBRE_USUARIO":
      return {
        ...state,
        nombreUsuario:action.payload, 
      };
    case "EDITAR_ARTICULO":
      return {
        ...state,
        idEditarArticulo:action.payload, 
      };

    case "CARGAR":    
      return {
        ...state,
        datos: action.payload,
        datosfiltrados:action.payload,
        subconjunto:action.payload,
      }
    case "UNARTI":    
      return {
        ...state,
        unarti:action.payload,
      }
    case "SearchXname":    
      return {
        ...state,
        datosfiltrados:action.payload,
      }
      case AUTH_REQ:
        return {...state,error:'',loading:true};
    
    case AUTH_SUCCESS:
        const data=action.payload;
        return {...state,error:'',loading:false,user:data};

    case AUTH_FAILURE:
        const error=action.payload;
        return {...state,loading:false,error:error};

    default:
      return state;
  }
};

export default rootReducer;
