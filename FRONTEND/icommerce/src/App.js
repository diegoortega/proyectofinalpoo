import React, { useState } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Login from './components/auth/Login';

import HeaderComponent from './components/header/header';
import FooterComponent from './components/footer/footer';
import DashboardComponent from './components/dashboard/dashboard';
import Aside from './components/aside/aside';
import NuevoUsuario from './components/NuevoUsuario';
import CuerpoComponent from './components/clientes/cuerpo/cuerpo';
import CardDetailComponent from './components/clientes/carddetail/cardDetail';
import Carrito from './components/ventas/Editar';
import ContextDetail from './components/context/ContextProvider';
import NavbarComponent from './components/header/navbarCliente';
import UpdateArticulo from './components/articulos/update'

function App() {
  const[bandera,setBandera] = useState(true)
  function activarNav (){
    setBandera(!bandera)
  }

  const[detalles, añadirDetalle] = useState([]);

  return (
    <ContextDetail>
    <Router>
      
      <Route exact path="/" >
              <HeaderComponent/>
              <Login activarNav={activarNav}/>  
      </Route>
          <main> 
            <Route exact path="/DashboardComponent" >
                  <HeaderComponent/>
                  <div className="contenedor-app">
                    <div className="aside"><Aside/></div>
                    
                      <div className="seccion-principal">
                        <DashboardComponent/>
                      </div>
                  </div>
            </Route>
          </main>
          <Route exact path="/Usuarios/NuevoUsuario">
            <HeaderComponent/>
            <NuevoUsuario/>
          </Route> 
          <Route exact path="/clientes">
            <NavbarComponent/>
            <CuerpoComponent/>
          </Route>
          {/* <Route exact path="/cardDetail/:id" component={CardDetailComponent}/> */}
          <Route exact path="/cardDetail/:id" render={(props) => (<> <NavbarComponent/><CardDetailComponent detalles={detalles} añadirDetalle={añadirDetalle}/></>)} />
          {/* <Route exact path="/ventas/Editar" component={Carrito}/> */}
          <Route exact path="/ventas/Editar" render={(props) => (<Carrito detallesCompra={detalles}/>)} />
          <Route exact path="/articulos/Editar" render={(props) => (<UpdateArticulo/>)} />

      <FooterComponent/>  
    </Router> 
    </ContextDetail>
  );
}

export default App;
