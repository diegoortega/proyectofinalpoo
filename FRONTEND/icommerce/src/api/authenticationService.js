import React from 'react';
import axios from 'axios';

//me permite  traer el token de localStorage
const getToken=()=>{
    return localStorage.getItem('USER_KEY');
}

//la peticion de login
export const userLogin=(authRequest)=>{
    return axios({
        'method':'POST',
        'url':"http://localhost:8080/auth/login",
        'data':authRequest
    })
}

//esta parte es una ves que el usuario este logueado
//primera parte aqui hay un ||
export const fetchUserData=()=>{
    return axios({
        method:'GET',
        url:"",
        
        //con esto nos permite aquirir la informacion ya que se nesecita un token
        headers:{
            'Authorization':'Bearer '+getToken()
        }
    })
}