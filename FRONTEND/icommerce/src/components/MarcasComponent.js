import React, { useEffect, useState } from 'react';

const MarcasComponent = ({marcas, articulo, guardarArticulo}) => {
    const change = (e) =>{
        guardarArticulo({
            ...articulo,
            [e.target.name]: e.target.value
        })
    }
    
    

    return ( 
        <select name="marcaId" id="marcaId" onChange={change} onSelect={change}>
            <option disabled selected >select</option>
            {marcas.map(marca => {
                return <option key={marca.id} value={marca.id}>{marca.nombre}</option>
            })}
        </select>
     );
}
 
export default MarcasComponent;