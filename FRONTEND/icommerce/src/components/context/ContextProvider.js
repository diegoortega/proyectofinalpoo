import React, {createContext, useState} from 'react';

//Creamos el context
export const ContextDetail = createContext();

//Provider: donde se encuentran las funciones y state
const ContextProvider = (props) => {
    const [detallesVenta, updateDetalles] = useState([]);

    return (
        <ContextDetail.Provider value={{detallesVenta, updateDetalles}}>
            {props.children}
        </ContextDetail.Provider>
    )
}

export default ContextProvider;