import React from 'react';

const RolesComponent = ({roles, usuario, guardarUsuario}) => {

    function modificarChar(e){
        console.log(e);
        guardarUsuario({
            ...usuario,
            rol: e
        })
    }

    return ( 
        <select name="rol" id="rolId" onChange={(event) => modificarChar(event.target.value)}  >
            {roles.map(rol => {
                return <option key={rol.nombre} value={rol.nombre}>{rol.nombre}</option>
            })}
        </select>
     );
}
 
export default RolesComponent;