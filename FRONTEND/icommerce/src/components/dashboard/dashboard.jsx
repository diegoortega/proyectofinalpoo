import React from "react";
import { connect } from "react-redux";
import GetListRoles from '../roles/GetListRoles';
import GetListArticulos from '../articulos/GetListArticulos';
import GetListUsuarios from '../usuarios/GetListUsuarios';
import GetListVentas from '../ventas/GetListVentas';
import NuevoArticulo from '../NuevoArticulo';
import RealizarVenta from '../ventas/Editar';
import UpdateArticulo from "../articulos/update";

function DashboardComponent({palabra}){
    var menu = <div className="conteiner justify-content-center" style={{display:'flex'}}> <img className="img-fluid" style={{maxWidth:'800px'}} src="https://app.svgator.com/assets/svgator.webapp/log-in-girl.svg" alt="" /></div>
   
    switch (palabra) {
        case 'Opciones':
            menu = <h1></h1>
            break;
        case 'Usuarios':
            menu = <GetListUsuarios/>
            break; 
        case 'Roles':
            menu = <GetListRoles/>
            break;
        case 'Articulos':
            menu = <GetListArticulos/>
            break;   
        case 'Ventas':
            menu = <GetListVentas/>
            break;
        case 'NuevoArticulo':
            menu = <NuevoArticulo/>
            break;
        case 'Carrito':
            menu = <RealizarVenta/>
            break;
        case 'EditarArticulo':
             menu = <UpdateArticulo/>
            break;
        default:
            break;
    }

    return (    
        <div>
            {
             menu
            }
        </div>
)

}

const mapStateToProps = state =>{
    return {    
        palabra:state.palabra
    }
  }
export default connect(mapStateToProps)(DashboardComponent)
