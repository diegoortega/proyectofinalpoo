import react,{useState} from 'react';
import { connect } from 'react-redux';
import { authenticate, authFailure, authSuccess } from '../../redux/authActions';
import './login.css'
import {Link} from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import {userLogin} from '../../api/authenticationService';
import {Alert,Spinner} from 'react-bootstrap';
import Carousel from 'react-bootstrap/Carousel';

import { SetNombreUsuario } from '../../actions/actions';

const Login=({loading,error,...props})=>{
    const history = useHistory();
    //son varibles donde se van a guardar 
    //los nombre y contrasenia para luego
    //enviar al login de backend
    const [values, setValues] = useState({
        username: '',
        password: ''
        });

    //metodo que es para enviar el
    //formulario 
    const handleSubmit=(evt)=>{
        evt.preventDefault();
        props.authenticate();

        userLogin(values).then((response)=>{
            //aqui tengo que poner  un  if 
            //si es que es admin me muestre una cosa
            //o otra
            console.log("response",response);
            if(response.status===200){
                
                props.setUser(response.data);
                localStorage.setItem('USER_NAME',response.data.username);
                //COMO EL NOMBRE SE ME PIERDE SI  USO  REDUX 
                //ENTONCES LA SOLU ES GUARDAR EN LOCAL STORAEG
                props.SetNombreUsuario(response.data.username);
                if(response.data.authorities[0].authority==='admin'){
                    history.push(`/DashboardComponent`);
                    //props.history.push('/cliente');

                }else if (response.data.authorities[0].authority==='user'){
                    history.push(`/clientes`);
                }
            }
            else{
               props.loginFailure('Something Wrong!Please Try Again'); 
            }


        }).catch((err)=>{

            if(err && err.response){
            
            switch(err.response.status){
                case 401:
                    console.log("401 status");
                    props.loginFailure("Authentication Failed.Bad Credentials");
                    break;
                default:
                    props.loginFailure('Something Wrong!Please Try Again'); 

            }

            }
            else{
                props.loginFailure('Something Wrong!Please Try Again');
            }
                

            

        });
        //console.log("Loading again",loading);

        
    }

    const handleChange = (e) => {
        e.persist();
        setValues(values => ({
        ...values,
        [e.target.name]: e.target.value
        }));
    };

    console.log("Loading ",loading);

    return (
        <>
            
        <div className="containerlogin">
            
            <div className="seccionIzquierdalogin">
                    <Carousel>
                        <Carousel.Item interval={500}>
                            <img
                            className="d-block imacarroll "
                            src="https://cdn.pixabay.com/photo/2018/06/30/09/29/monkey-3507317_960_720.jpg"
                            alt="First slide"
                            />
                            <Carousel.Caption>
                            <h3>Siente LAS VIBRAS!!</h3>
                            
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item interval={500}>
                            <img
                            className="d-block imacarroll "
                            src="https://cdn.pixabay.com/photo/2018/07/28/11/08/guitar-3567767_960_720.jpg"
                            alt="Second slide"
                            />
                            <Carousel.Caption>
                            <h3>Escucha tu corazon </h3>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block imacarroll "
                            src="https://cdn.pixabay.com/photo/2015/04/15/09/47/men-723557_960_720.jpg"
                            alt="Third slide"
                            />
                            <Carousel.Caption>
                            <h3>Comparte Experiencia Con Amigos</h3>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>
            </div>
            <div className="seccionDerechalogin">
                <h2 className="h2login">Bienvenido !!</h2>
                <div className="contenedor-form sombra-dark">
                    <h1>Iniciar sesión</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="campo-form">
                            <label htmlFor="usuario">Usuario</label>
                            <input
                                type="text"     
                                id="usuario"
                                name="username"
                                placeholder="username"
                                value={values.username} 
                                onChange={handleChange}
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="password">Password</label>
                            <input
                                type="password"
                                id="password"
                                name="password"
                                placeholder="Password"
                                value={values.password} 
                                onChange={handleChange}
                            />
                        </div>
                        <div className="form-group m-0">
                                                <button type="submit" className="btn btn-dark">
                                                    Login
                                                    {loading && (
                                                        <Spinner
                                                        as="span"
                                                        animation="border"
                                                        size="sm"
                                                        role="status"
                                                        aria-hidden="true"
                                                    />
                                                    )}
                                                
                                                </button>
                                            </div>
                    </form>
                    { error &&
                                        <Alert style={{marginTop:'20px'}} variant="danger">
                                                {error}
                                            </Alert>

                                        }
                    <Link to={'/Usuarios/NuevoUsuario'} className="enlace-cuenta">Registrar Usuario</Link>
                </div>
            </div>

        </div>

       </>
    )


    
}

const mapStateToProps = state =>{
    return {
        loading:state.loading,
        error:state.error
    }
}


const mapDispatchToProps=(dispatch)=>{

    return {
        authenticate :()=> dispatch(authenticate()),
        setUser:(data)=> dispatch(authSuccess(data)),
        loginFailure:(message)=>dispatch(authFailure(message)),
        SetNombreUsuario:(palabra)=> dispatch(SetNombreUsuario(palabra)),
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(Login);