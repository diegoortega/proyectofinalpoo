import React, { useContext ,useEffect} from "react";
import { Link } from "react-router-dom";
import './deatil.css';
import { useParams } from "react-router";
import Mini from "./miniatura";
import { getunArti } from "../../../actions/actions";
import { connect } from "react-redux";
import { ContextDetail }from '../../context/ContextProvider';
import Carousel from 'react-bootstrap/Carousel';
import Swal from "sweetalert2";

function CardDetailComponent ({ getunArti,unarti}){
        //imagen por defecto
        const {id}=useParams()
        const {detallesVenta, updateDetalles} = useContext(ContextDetail)
        
        useEffect(() => {
            getunArti(id) ; 
        },[]);

        const almacenar = () => {
            
            updateDetalles([...detallesVenta, unarti]);
            Swal.fire({
                icon: "success",
                title: "Agregado en carrito",
                showConfirmButton: false,
                timer: 1000,
            });
        }

        const imgpordefect = "https://thumbs.dreamstime.com/b/sin-foto-ni-icono-de-imagen-en-blanco-cargar-im%C3%A1genes-o-falta-marca-no-disponible-pr%C3%B3xima-se%C3%B1al-silueta-naturaleza-simple-marco-215973362.jpg" 
       
        
        return (
        
            <>
                <div className ="cuerpodetail ">
                   <div className="container my-4 agarratodo">
                    <div class="row justify-content-md-center">

                            <div class="col col-lg-1 ">
                            
                                <Mini imgpordefect = { unarti.imagenBase1 }/>
                                <Mini imgpordefect = { unarti.imagenBase2 }/>
                               
                            </div>

                            <div class="col-md-8 partemedia">
                            <img  className="grande img-fluid" src={unarti.imagenBase?`data:image/jpeg;base64,${unarti.imagenBase}`:imgpordefect } alt="" />
                                <br/>
                                <h4>DESCRIPCION</h4>
                                <p>{unarti.descripcion}</p>
                            </div>

                            <div class="col col-lg-3 rounded border colodetail ">
                                <div className="descripderecho">
                                    <h2 >{unarti.nombre}</h2>
                                    <br/>
                                    <p>marca : {unarti.marcaNombre} </p>
                                    <br/>
                                    <p>precio : {unarti.precio} </p>
                                    <br/>
                                    <p>en stock  : {unarti.stock} </p>
                                    <br/>
                                    
                                    <Link to="/clientes" className="btn btn-dark" onClick={almacenar} >Comprar</Link>
                                    <Link to="/clientes" className="btn btn-light">Cancelar</Link>
                                </div>
                            </div>
                    </div>
                    </div>
                </div>
                <div className ="cuerpodetailMovil">
                   

                            <div>
                                <Carousel>
                                    <Carousel.Item interval={500}>
                                        <img
                                        className="d-block  img-fluid"
                                        src={unarti.imagenBase?`data:image/jpeg;base64,${unarti.imagenBase}`:imgpordefect }
                                        alt="First slide"
                                        />
                                        <Carousel.Caption>
                                        
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                    <Carousel.Item interval={500}>
                                        <img
                                        className="d-block  img-fluid"
                                        src={unarti.imagenBase2?`data:image/jpeg;base64,${unarti.imagenBase}`:imgpordefect }
                                        alt="Second slide"
                                        />
                                        <Carousel.Caption>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                    <Carousel.Item>
                                        <img
                                        className="d-block  img-fluid"
                                        src={unarti.imagenBase1?`data:image/jpeg;base64,${unarti.imagenBase}`:imgpordefect }
                                        alt="Third slide"
                                        />
                                        <Carousel.Caption>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                </Carousel>
                               
                            </div>

                           

                            <div >
                                <div className="descripderechoMovil">
                                    <h2 >{unarti.nombre}</h2>
                                    <br/>
                                    <p>marca : {unarti.marcaNombre} </p>
                                    <br/>
                                    <p>precio : {unarti.precio} </p>
                                    <br/>
                                    <p>en stock  : {unarti.stock} </p>
                                    <br/>
                                    
                                    <Link to="/ventas/Editar" className="btn btn-dark" onClick={almacenar} >Comprar</Link>
                                    <Link to="/clientes" className="btn btn-light">Cancelar</Link>
                                </div>
                                
                            </div>
                            <div className="descripderechoMovil">
                            
                                <br/>
                                    <h4>DESCRIPCION</h4>
                                    <p>{unarti.descripcion}</p>
                            </div>
                    
                    
                </div>
                
            </> 
        );
    

}

const mapStateToProps = state =>{
    return {
        unarti:state.unarti
    }
  }
  
export default connect(mapStateToProps ,{getunArti})(CardDetailComponent)
