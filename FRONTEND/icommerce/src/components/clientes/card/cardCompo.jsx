import React from "react";
import { Link } from "react-router-dom";
import c from './cadcompo.module.css'

export default function CardComponent (props){
        //imagen por defecto
        const imgpordefect = "https://thumbs.dreamstime.com/b/sin-foto-ni-icono-de-imagen-en-blanco-cargar-im%C3%A1genes-o-falta-marca-no-disponible-pr%C3%B3xima-se%C3%B1al-silueta-naturaleza-simple-marco-215973362.jpg" 
       
        
        return (
        
            <>
            <div className = {c.name1}>
                   
                    <Link to={`/cardDetail/${props.objeto.id}`}   >
                        <p  className ={c.name} >{props.objeto.nombre}</p>  
                                     
<img className ={c.pic} src={props.objeto.imagenBase?`data:image/jpeg;base64,${props.objeto.imagenBase}`:imgpordefect } alt={"img"}/>
                    <br/>

                    <div className = {c.divDietsNames}>
                        <h4>precio : {props.objeto.precio}</h4>  
                    </div> 
                    <div className = {c.divDietsNames}>
                        <h4>marca : {props.objeto.marcaNombre}</h4>  
                    </div>     
                    </Link>   
                    
                </div>
             
                
            </> 
        );
    

}
