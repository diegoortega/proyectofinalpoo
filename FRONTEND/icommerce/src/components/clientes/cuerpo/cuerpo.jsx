import React, { useEffect } from "react";
import SectionComponent from "../secction/seccion";
import SideBar from "../sideBar/sideBar";
import './cuerpo.css';
import { getArticulos } from "../../../actions/actions";
import { connect } from "react-redux";
import FlotanteComponent from "../sideBar/flotanteSidebar";
function CuerpoComponent ({ getArticulos }){
        //imagen por defecto
        useEffect(() => {
                getArticulos(); 
        },[]);
        
        return (
            <>
            <div  className="cuerpo">
                    <div className="asideCliente">
                        <SideBar/>
                    </div>
                    <SectionComponent/>
                    
            </div>
            <FlotanteComponent/>
            </>
        );
    

}

export default connect(null,{ getArticulos })(CuerpoComponent)