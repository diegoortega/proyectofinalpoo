import React, { useState } from "react";
import "./sideBar.css";
import { searchArtiXname } from "../../../actions/actions";
import { connect } from "react-redux";
import { Navbar,Container,Offcanvas,Nav } from 'react-bootstrap';

function SideBar ({ searchArtiXname }){
    
    
    const [valorBuscar,setValorBuscar] = useState('');
    //siempre crear un set por que sino no funciona

    function onSubmitForm(e){   
        e.preventDefault(); 
        if(valorBuscar!==''){
            searchArtiXname(valorBuscar)
            setValorBuscar('')
        }
        
    }
    return(
        <>
        <div className="buscador text-light ">
            <div className="sideBar">
           
                <ul>
                    <li>
                        <form className="d-flex ">
                            <input style={{marginLeft:'0'}} className="todos"
                             type="search" 
                             placeholder="Search"
                             aria-label="Search"
                             value={valorBuscar} 
                             onChange={(e)=>setValorBuscar(e.target.value)}/>
                            <button style={{marginLeft:'0'}} className="todos" onClick={(e)=>onSubmitForm(e)} ><i class="bi bi-search"></i></button>
                        </form>
                    <br />
                  
                    </li>

                </ul>
            </div>
            

        </div>
        

        </>
    );

}
export default connect(null,{ searchArtiXname })(SideBar)