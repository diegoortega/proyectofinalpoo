import React from "react";
import "./sideBar.css";
import { useState } from "react";
import { searchArtiXname } from "../../../actions/actions";
import { Navbar ,Container,Offcanvas ,Nav} from "react-bootstrap";
import { connect } from "react-redux";
function FlotanteComponent({ searchArtiXname }){
    const [valorBuscar,setValorBuscar] = useState('');
    //siempre crear un set por que sino no funciona

    function onSubmitForm(e){   
        e.preventDefault(); 
        if(valorBuscar!==''){
            searchArtiXname(valorBuscar)
            setValorBuscar('')
        }
        
    }
    return (    
        <>

            <div className="float-nav">
                    
                <Navbar bg="light" expand={false} className="flotanSidear" >
                    <Container fluid>
                    <Navbar.Brand  href="#"></Navbar.Brand>
                    <Navbar.Toggle  aria-controls="basic-navbar-nav"><i className="bi bi-funnel-fill"></i></Navbar.Toggle>
                    <Navbar.Offcanvas
                        id="offcanvasNavbar"
                        aria-labelledby="offcanvasNavbarLabel"
                        placement="end"
                    >
                        <Offcanvas.Header closeButton>
                        <Offcanvas.Title id="offcanvasNavbarLabel">Hola</Offcanvas.Title>
                        </Offcanvas.Header>
                        <Offcanvas.Body>
                        <Nav className="justify-content-end flex-grow-1 pe-3">
                            <form className="d-flex ">
                                <input style={{marginLeft:'0'}} className="todos"
                                type="search" 
                                placeholder="Search"
                                aria-label="Search"
                                value={valorBuscar} 
                                onChange={(e)=>setValorBuscar(e.target.value)}/>
                                <button style={{marginLeft:'0'}} className="todos" onClick={(e)=>onSubmitForm(e)} ><i class="bi bi-search"></i></button>
                            </form>
                        </Nav>
                        
                        
                        </Offcanvas.Body>
                    </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            </div>

          
        </>
)

}

export default connect(null,{ searchArtiXname })(FlotanteComponent)