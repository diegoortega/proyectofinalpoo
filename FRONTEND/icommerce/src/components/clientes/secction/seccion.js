
import CardComponent from "../card/cardCompo";
import pag from "./seccion.module.css";
import { connect } from "react-redux";
function SectionComponent({datosfiltrados}){

    return (        
        <section className={pag.containerPag}>
                {datosfiltrados.map((element)=>(
                  
                  <div key="element.id" >  
                            <CardComponent
                            objeto ={element}
                            /> 
                    </div>
                )
              )}
       
        </section>
)
}


const mapStateToProps = state =>{
        return {
          datosfiltrados:state.datosfiltrados
        }
      }
      
export default connect(mapStateToProps)(SectionComponent)