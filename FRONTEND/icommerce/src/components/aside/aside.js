import React from 'react';
import {Link} from 'react-router-dom';
import { ELejirSidebar } from '../../actions/actions';
import { connect } from "react-redux";
import './aside.css'
import { Button} from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Navbar,Container ,Nav ,Offcanvas,NavDropdown} from 'react-bootstrap';

const Aside = ({nombreUsuario, ELejirSidebar }) => {
  const history = useHistory();
  const logOut=()=>{

    localStorage.clear();
    history.push(`/`);

  }
  function modificarChar(e){
    ELejirSidebar(e)
  }
  const username =localStorage.getItem('USER_NAME');
    return ( 
        <>
          <aside className="asideGrande">
            <h1 >Bienvenid@  !!</h1>
            <span style={{display:'flex', alignItems: 'center' ,justifyContent:'center',flexDirection:'column',backgroundColor:'black' ,borderRadius:'12px',width:'70px',margin: 'auto'}}><img 
            width="50px"
            height="50px" 
            src="https://cdn.quasar.dev/img/boy-avatar.png"/>
            <h3 style={{fontSize:'30px'}}>{username}</h3>
            </span>
            <div className="asaidecontainer">
              <button className="busc" value = "Usuarios" onClick={(event) => modificarChar(event.target.value)} >Usuarios</button>
              <button className="busc" value = "Articulos" onClick={(event) => modificarChar(event.target.value)} >Artículos</button>
              <button className="busc" value = "Ventas" onClick={(event) => modificarChar(event.target.value)} >Ventas</button>
              <button className="busc" value = "NuevoArticulo" onClick={(event) => modificarChar(event.target.value)} >NuevoArticulo</button>
              <button className="busc" value = "Carrito" onClick={(event) => modificarChar(event.target.value)}>Carrito</button>
              <Link className="busc" to={'/clientes'} >Vista Cliente</Link>
              <Button className="btn btn-dark" style={{marginTop:'5px'}} onClick={() =>logOut()}>Logout</Button>

            </div>
          
          </aside>
          <Navbar bg="light" expand={false} className="asidePequenio">
            <Container fluid>
              <Navbar.Brand  href="#">Elija Una Opcion</Navbar.Brand>
              <Navbar.Toggle aria-controls="offcanvasNavbar" />
              <Navbar.Offcanvas
                id="offcanvasNavbar"
                aria-labelledby="offcanvasNavbarLabel"
                placement="end"
              >
                <Offcanvas.Header closeButton>
                  <Offcanvas.Title id="offcanvasNavbarLabel">Hola</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                  <Nav className="justify-content-end flex-grow-1 pe-3">
                    <h1 >Bienvenid@  !!</h1>
                    <span style={{display:'flex', alignItems: 'center' ,justifyContent:'center',flexDirection:'column',backgroundColor:'black' ,borderRadius:'12px',width:'70px',margin: 'auto'}}><img 
                    width="50px"
                    height="50px" 
                    src="https://cdn.quasar.dev/img/boy-avatar.png"/>
                    <h3 style={{fontSize:'30px'}}>{username}</h3>
                    </span>
                    <button className="busc" value = "Usuarios" onClick={(event) => modificarChar(event.target.value)} >Usuarios</button>
                    <button className="busc" value = "Articulos" onClick={(event) => modificarChar(event.target.value)} >Artículos</button>
                    <button className="busc" value = "Ventas" onClick={(event) => modificarChar(event.target.value)} >Ventas</button>
                    <button className="busc" value = "NuevoArticulo" onClick={(event) => modificarChar(event.target.value)} >NuevoArticulo</button>
                    <button className="busc" value = "Carrito" onClick={(event) => modificarChar(event.target.value)}>Carrito</button>
                    <Link className="busc" to={'/clientes'} >Vista Cliente</Link>
                    <Button className="btn btn-dark" style={{marginTop:'5px'}} onClick={() =>logOut()}>Logout</Button>
                    
                  </Nav>
                  
                
                </Offcanvas.Body>
              </Navbar.Offcanvas>
            </Container>
          </Navbar>
        </>
     ); 
}

const mapStateToProps = state =>{
  return {
    nombreUsuario:state.nombreUsuario
  }
}
export default connect(mapStateToProps,{ ELejirSidebar })(Aside)