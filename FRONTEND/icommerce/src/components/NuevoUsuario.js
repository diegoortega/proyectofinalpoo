import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import RolesComponent from './RolesComponent';
import url from '../urls.json';
import Carousel from 'react-bootstrap/Carousel';
import Swal from "sweetalert2";
import { useHistory } from 'react-router-dom';

const NuevoUsuario = () => {
    const history = useHistory();
    const [ usuario, guardarUsuario ] = useState({
        nombre: '',
        username: '',
        email: '',
        password: '',
        confirmacion: '',
        rol: 'admin'
    });

    const {nombre, username, email, password, confirmacion, rol} = usuario;

    const onChange = (e) =>{
        guardarUsuario({
            ...usuario,
            [e.target.name]: e.target.value
        })
    }

    const [ listaRol, setearListaRol ] = useState([]);
    
    

    const getListRol = () => {
        axios.get(`${process.env.hostUrl||url.baseUrl}roles/getList/`).then(response => {
            setearListaRol(response.data);
            
            
        })
    };
      
    useEffect( () => {
        getListRol();
      
    }, [])

    const save = () => {
        console.log({nombre: nombre,
            username: username,
            email: email,
            password: password,
            rol: rol});
        axios.post(url.baseUrl + "auth/nuevo", {
            nombre: nombre,
            username: username,
            email: email,
            password: password,
            rol: rol
        }).then(response => {
          console.log(response);
        }).catch(error =>{
            console.log(error.message);
        })
      };

    const onSubmit = (e) =>{
        e.preventDefault();
        save();
        Swal.fire({
            icon: "success",
            title: "Guardado correctamente",
            showConfirmButton: false,
            timer: 2000,
        });
        guardarUsuario({
            nombre: '',
            username: '',
            email: '',
            password: '',
            confirmacion: '',
            rol: 'admin',
        });
        history.push(`/`);
        
        
    }

    return ( 
        <div className="containerlogin">
            <div className="seccionIzquierdalogin">
                    <Carousel>
                        <Carousel.Item interval={500}>
                            <img
                            className="d-block imacarroll "
                            src="https://cdn.pixabay.com/photo/2018/06/30/09/29/monkey-3507317_960_720.jpg"
                            alt="First slide"
                            />
                            <Carousel.Caption>
                            <h3>Siente LAS VIBRAS!!</h3>
                            
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item interval={500}>
                            <img
                            className="d-block imacarroll "
                            src="https://cdn.pixabay.com/photo/2018/07/28/11/08/guitar-3567767_960_720.jpg"
                            alt="Second slide"
                            />
                            <Carousel.Caption>
                            <h3>Escucha tu corazon </h3>
                            </Carousel.Caption>
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block imacarroll "
                            src="https://cdn.pixabay.com/photo/2015/04/15/09/47/men-723557_960_720.jpg"
                            alt="Third slide"
                            />
                            <Carousel.Caption>
                            <h3>Comparte Experiencia Con Amigos</h3>
                            </Carousel.Caption>
                        </Carousel.Item>
                    </Carousel>
            </div>
        
            <div className="">
                <div className="contenedor-form sombra-dark">
                    <h1>Crea tu Cuenta de Usuario</h1>
                    <form onSubmit={onSubmit}>
                        <div className="campo-form">
                            <label htmlFor="nombre">Nombre</label>
                            <input
                                type="text"
                                id="nombre"
                                name="nombre"
                                placeholder="Nombre"
                                value={nombre}
                                onChange={onChange}
                                required
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="username">Usuario</label>
                            <input
                                type="text"
                                id="username"
                                name="username"
                                placeholder="Usuario"
                                value={username}
                                onChange={onChange}
                                required
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="email">Correo</label>
                            <input
                                type="email"
                                id="email"
                                name="email"
                                placeholder="Correo"
                                value={email}
                                onChange={onChange}
                                required
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="rolId">Rol</label>
                            
                            <RolesComponent roles = {listaRol} usuario={usuario} guardarUsuario={guardarUsuario}/>
                        </div>
                        
                        <div className="campo-form">
                            <label htmlFor="password">Password</label>
                            <input
                                type="password"
                                id="password"
                                name="password"
                                placeholder="Password"
                                value={password}
                                onChange={onChange}
                                required
                            />
                        </div>
                        <div className="campo-form">
                            <label htmlFor="password">Confirmar Password</label>
                            <input
                                type="password"
                                id="confirmacion"
                                name="confirmacion"
                                placeholder="Confirmar Password"
                                value={confirmacion}
                                onChange={onChange}
                                required
                            />
                        </div>
                        <div className="campo-form">
                            <input type="submit" className="btn btn-dark" value="ACEPTAR"/>
                        </div>
                    </form>
                    <Link to={'/'} className="enlace-cuenta">Volver</Link>
                </div>
            </div>
        </div>
     );
}
 
export default NuevoUsuario;