import React, {useState,useEffect} from 'react';
import axios from 'axios';
import url from "../../urls.json";
import {Card,Container,ListGroup,ListGroupItem} from 'react-bootstrap';
const GetListVentas = () => {

    const [ventas ,setVentas]=useState([]);
 
    var cont = 0; 
    var cont2 = 0; 
    useEffect(()=>{
        peticionget();
    },[])

    const getToken=()=>{
        return localStorage.getItem('USER_KEY');
    }
    function peticionget(){
            axios({
                method:'GET',
                url:`${process.env.hostUrl||url.baseUrl}ventas/getList`,
                
                //con esto nos permite aquirir la informacion ya que se nesecita un token
                headers:{
                    'Authorization':'Bearer '+getToken()
                }
            }).then(res=>{
                    setVentas(res.data);
                    console.log(res.data);
                })  
                .catch(e=>{
                    console.log(e)
                })
    }


    return ( 
        <div>
            <h1>Listado Ventas</h1>
            <table className="table" id="tableListArticulo">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">E-Mail</th>
                        <th scope="col">Fecha</th>
                    </tr>
                </thead>
                <tbody>
                
                {ventas.map((e)=>{
                     
                    cont = cont + 1 ;
                    return (
                        
                        <tr key={e.id} >
                            <th scope="row" >{cont}</th>
                            <td >{e.nombre}</td>
                            <td>{e.correo}</td>
                            <td>{e.fecha}</td>
                        </tr>
                        )}
                )}
                                
                </tbody>
            </table>

            <div className="MovilListArticulos">
                
                {ventas.map((e)=>{
                     
                     cont2 = cont2 + 1 ;
                     return (
                        
                        <Card className="my-3" style={{ width: '18rem' }}>
                        
                        <Card.Body>
                            <Card.Title className="text-dark">{e.nombre}</Card.Title>
                            
                        </Card.Body>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem><span>producto :</span> {cont}</ListGroupItem>

                            <ListGroupItem><Card.Text className="text-dark" >
                            <span>correo :</span> {e.correo}
                            </Card.Text></ListGroupItem>
    
                            <ListGroupItem><span>fecha :</span> {e.fecha}</ListGroupItem>
                            
                        </ListGroup>
                       
                        </Card>
                         
                         )}
                 )}
                  
            </div>

        </div>
     );
}
 
export default GetListVentas;