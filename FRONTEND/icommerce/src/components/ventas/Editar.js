import React, {useState, useEffect, useContext} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import url from '../../urls.json';
import { ContextDetail } from '../context/ContextProvider';

import { connect } from 'react-redux';
import { ELejirSidebar } from '../../actions/actions';
import Swal from "sweetalert2";
const RealizarVenta = ( { ELejirSidebar }) => {
    
    const {detallesVenta, updateDetalles} = useContext(ContextDetail);
   
    const [ venta, guardarVenta ] = useState({
        nombre: '',
        correo: '',
        fecha: '',
        detalles: detallesVenta
    });
    
    const {nombre, correo, fecha, detalles } = venta;
    console.log(detalles);
    const onChange = (e) =>{
        guardarVenta({
            ...venta,
            [e.target.name]: e.target.value
        })
    }

    useEffect( () => {
    }, [])

    const getToken=()=>{
        return localStorage.getItem('USER_KEY');
    }

    const save = () => {
        axios({
            method:'POST',
            url:`${process.env.hostUrl||url.baseUrl}ventas/save`,
            data: {
                nombre: nombre,
                correo: correo,
                fecha: fecha,
                detalles: detallesVenta
            },
            //con esto nos permite aquirir la informacion ya que se nesecita un token
            headers:{
                'Authorization':'Bearer '+getToken()
            }
        }).then(response => {
          console.log(response);
        }).catch(error =>{
            console.log(error.message);
        })
      };

    const onSubmit = (e) =>{
        e.preventDefault();
        save();
        Swal.fire({
            icon: "success",
            title: "Guardado correctamente",
            showConfirmButton: false,
            timer: 1000,
        });
        guardarVenta({
            nombre: '',
            correo: '',
            fecha: '',
            detalles: []
        });
        ELejirSidebar("Ventas");
          
    }
    return ( 
        <div className="form-usuario">
            <div className="contenedor-form sombra-dark">
                <h1>CARRITO</h1>
                
                <form onSubmit={onSubmit}>
                    <div className="campo-form">
                        <label htmlFor="nombre">Nombre</label>
                        <input
                            type="text"
                            id="nombre"
                            name="nombre"
                            placeholder={nombre}
                            value={nombre}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="correo">Correo</label>
                        <input
                            type="email"
                            id="correo"
                            name="correo"
                            placeholder={correo}
                            value={correo}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="fecha">Fecha</label>
                        <input
                            type="date"
                            id="fecha"
                            name="fecha"
                            placeholder={fecha}
                            value={fecha}
                            onChange={onChange}
                        />
                    </div>
                    
                    <div className="campo-form">
                        <label htmlFor="detalles">Detalles</label>
                        <div>
                            {Object.keys(detalles).map (item => {
                                return (
                                <div> {detalles[item].nombre}</div>
                                )})}   
                        </div>
                    </div>
                    <div className="campo-form">
                        <input type="submit" className="btn btn-dark" value="Comprar"/>
                    </div>
                </form>
            </div>
        </div>
     );
}
export default connect(null,{ ELejirSidebar })(RealizarVenta);
