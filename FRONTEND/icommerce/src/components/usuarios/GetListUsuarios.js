import React,{useState,useEffect} from "react";
import axios from 'axios';
import { delPersona } from "../controller/peticiones";
import url from "../../urls.json";
import {Link} from 'react-router-dom';

const GetListUsuarios = () => {
    const [clientes ,setClientes]=useState([]);
 
    var cont = 0; 
    useEffect(()=>{
        peticionget();
    },[])
    const getToken=()=>{
        return localStorage.getItem('USER_KEY');
    }

    function peticionget(){
        console.log(url.baseUrl);
            axios({
                method:'GET',
                url:`${process.env.hostUrl||url.baseUrl}usuarios/getList/`,
                
                //con esto nos permite aquirir la informacion ya que se nesecita un token
                headers:{
                    'Authorization':'Bearer '+getToken()
                }
            }).then(res=>{
                    setClientes(res.data);
                })  
                .catch(e=>{
                    console.log(e)
                })
    }
    function eliminar(id){
        delPersona(id);
    }

    return ( 
        <div>
            <h1>Listado Usuarios</h1>
            <Link to={'/Usuarios/NuevoUsuario'} className="enlace-cuenta">Nuevo Usuario</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Usuario</th>
                        <th scope="col">E-Mail</th>
                        <th scope="col">rol</th>
                    </tr>
                </thead>
                <tbody>
                
                {clientes.map((e)=>{
                     
                    cont = cont + 1 ;
                    return (
                        
                        <tr key={e.id} >
                            <th scope="row" >{cont}</th>
                            <td >{e.nombre}</td>
                            <td>{e.email}</td>
                            <td>{e.rolNombre}</td>
                            
                        </tr>
                        )}
                )}
                      
                  
                  
                </tbody>
            </table>

        </div>
     );
}
 
export default GetListUsuarios;