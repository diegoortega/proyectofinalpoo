import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import MarcasComponent from '../MarcasComponent';
import url from '../../urls.json';
import Swal from "sweetalert2";
import { connect } from 'react-redux';
import { ELejirSidebar } from '../../actions/actions';


const UpdateArticulo = ({idEditarArticulo, ELejirSidebar }) => {

    const getToken=()=>{
        return localStorage.getItem('USER_KEY');
    }
    const [ articulo, guardarArticulo ] = useState({
        id:0,
        nombre: '',
        descripcion: '',
        precio: 0,
        stock: 0,
        imagenPath: '',
        imagenPathAux1: '',
        imagenPathAux2: '',
        marcaId: 1
    });

    
    const {id,nombre, descripcion, precio, stock, imagenPath, imagenPathAux1, imagenPathAux2, marcaId} = articulo;

    const onChange = (e) =>{
        guardarArticulo({
            ...articulo,
            [e.target.name]: e.target.value
        })
    }

    const [ listaMarca, setearListaMarca ] = useState([]);

    const getListaMarca = () => {
        axios({
            method:'GET',
            url:`${process.env.hostUrl||url.baseUrl}marcas/getList/`,
            
            //con esto nos permite aquirir la informacion ya que se nesecita un token
            headers:{
                'Authorization':'Bearer '+getToken()
            }
        }).then(response => {
            setearListaMarca(response.data);
        })
      };
    
    //Buscando el objeto a editar
    const buscarObjeto = (idB) => {
        axios({
            method:'GET',
            url:`${process.env.hostUrl||url.baseUrl}articulos/getList/${idB}`,
            
            //con esto nos permite aquirir la informacion ya que se nesecita un token
            headers:{
                'Authorization':'Bearer '+getToken()
            }
        }).then(response => {
            guardarArticulo({
                ...articulo,
                id: response.data.id,
                nombre: response.data.nombre,
                precio: response.data.precio,
                stock: response.data.stock,
                descripcion: response.data.descripcion,
                imagenPath: response.data.imagenPath!=null?response.data.imagenPath:'',
                imagenPathAux1: response.data.imagenPathAux1!=null?response.data.imagenPath1:'',
                imagenPathAux2: response.data.imagenPathAux2!=null?response.data.imagenPath2:'',
                marcaId: response.data.marcaId

            })
            
            
        })
      };
      
    useEffect( () => {
        getListaMarca();
        buscarObjeto(idEditarArticulo);
        
    }, [])

    

    const save = () => {
        axios({
            method:'POST',
            url:`${process.env.hostUrl||url.baseUrl}articulos/save`,
            data: {
                id:id,
                nombre: nombre,
                descripcion: descripcion,
                precio: precio,
                stock: stock,
                imagenPath: imagenPath,
                imagenPathAux1: imagenPathAux1,
                imagenPathAux2: imagenPathAux2,
                marcaId: marcaId
            },
            //con esto nos permite aquirir la informacion ya que se nesecita un token
            headers:{
                'Authorization':'Bearer '+getToken()
            }
        }).then(response => {
          console.log(response);
        }).catch(error =>{
            console.log(error.message);
        })
      };

    const onSubmit = (e) =>{
        e.preventDefault();
        save();
        Swal.fire({
            icon: "success",
            title: "Guardado correctamente",
            showConfirmButton: false,
            timer: 1000,
        });

        //alert("guardado correctamente")
        guardarArticulo({
            nombre: '',
            descripcion: '',
            precio: 0.,
            stock: 0,
            imagenPath: '',
            imagenPathAux1: '',
            imagenPathAux2:'',
            marcaId: 1
        });
        ELejirSidebar("Articulos");
    }

    return ( 
        <div className="artform form-articulo">
            
            <div className="contenedor-form sombra-dark">
                <h1>Crea/Edita un artículo</h1>
                <form onSubmit={onSubmit}>
                    <div className="campo-form">
                        <label htmlFor="nombre">Nombre</label>
                        <input
                            type="text"
                            id="nombre"
                            name="nombre"
                            placeholder={nombre}
                            value={nombre}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="descripcion">Descripción</label>
                        <input
                            type="text"
                            id="descripcion"
                            name="descripcion"
                            placeholder={descripcion}
                            value={descripcion}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="precio">Precio</label>
                        <input
                            type="number"
                            id="precio"
                            name="precio"
                            placeholder={precio}
                            value={precio}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="articuloId">Articulo</label>
                        <MarcasComponent marcas = {listaMarca} articulo={articulo} guardarArticulo={guardarArticulo}/>
                    </div>
                    
                    <div className="campo-form">
                        <label htmlFor="stock">Stock</label>
                        <input
                            type="number"
                            id="stock"
                            name="stock"
                            placeholder={stock}
                            value={stock}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="imagenPath">Imagen</label>
                        <input
                            type="file"
                            id="imagenPath"
                            name="imagenPath"
                            placeholder="Imagen"
                            value={imagenPath}
                            onSelect={onChange}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="imagenPathAux1">Imagen Muestra 1</label>
                        <input
                            type="file"
                            id="imagenPathAux1"
                            name="imagenPathAux1"
                            placeholder="Imagen"
                            value={imagenPathAux1}
                            onSelect={onChange}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="imagenPathAux2">Imagen Muestra 2</label>
                        <input
                            type="file"
                            id="imagenPathAux2"
                            name="imagenPathAux2"
                            placeholder="Imagen"
                            value={imagenPathAux2}
                            onSelect={onChange}
                            onChange={onChange}
                        />
                    </div>
                    <div className="campo-form">
                        <input type="submit" className="btn btn-dark" value="Dar de alta"/>
                    </div>
                </form>
               
            </div>
        </div>
     );
}

const mapStateToProps = state =>{
    return {
        idEditarArticulo:state.idEditarArticulo
    }
}
export default connect(mapStateToProps,{ ELejirSidebar })(UpdateArticulo);