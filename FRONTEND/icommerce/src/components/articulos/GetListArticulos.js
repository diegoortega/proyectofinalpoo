import React,{useState,useEffect} from "react";
import axios from 'axios';
import url from "../../urls.json";
import {Link} from 'react-router-dom';
import { ELejirSidebar,SetIdEditArticulo } from '../../actions/actions';
import { connect } from "react-redux";
import Swal from "sweetalert2";
import {Card,Container,ListGroup,ListGroupItem} from 'react-bootstrap';
import './articulolist.css';
const GetListArticulos = ({ELejirSidebar,SetIdEditArticulo }) => {
    const [articulos ,setArticulos]=useState([]);
 
    var cont = 0; 
    useEffect(()=>{
        peticionget();
    },[])

    const getToken=()=>{
        return localStorage.getItem('USER_KEY');
    }
    function editar(idB){
        ELejirSidebar("EditarArticulo");
        SetIdEditArticulo(idB);
    }

    function peticionget(){
        axios({
            method:'GET',
            url:`${process.env.hostUrl||url.baseUrl}articulos/getList/`,
            
            //con esto nos permite aquirir la informacion ya que se nesecita un token
            headers:{
                'Authorization':'Bearer '+getToken()
            }
        }).then(res=>{
                setArticulos(res.data);
            })  
            .catch(e=>{
                console.log(e)
            })
    }
    function eliminar(idB){
        axios({
            method:'GET',
            url:`${process.env.hostUrl||url.baseUrl}articulos/eraseArticulo/${idB}`,
            
            //con esto nos permite aquirir la informacion ya que se nesecita un token
            headers:{
                'Authorization':'Bearer '+getToken()
            }
        }).then(res=>{
                Swal.fire({
                    icon: "success",
                    title: "Eliminado correctamente",
                    showConfirmButton: false,
                    timer: 1000,
                });
                peticionget();
            })  
            .catch(e=>{
                console.log(e)
            })
    }


    return ( 
        <div>
            <h1>Listado Articulos</h1>
            <Link to={'/Articulos/NuevoArticulo'} className="enlace-cuenta">Nuevo Articulo</Link>
            <table className="table" id="tableListArticulo">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Descripción</th>
                        <th scope="col">Imagen</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                
                {articulos.map((e)=>{
                     
                    cont = cont + 1 ;
                    return (
                        
                        <tr key={e.id} >
                            <th scope="row" >{cont}</th>
                            <td >{e.nombre}</td>
                            <td>{e.descripcion}</td>
                            <td>
                                <img width='20%' src={`data:image/jpeg;base64,${e.imagenBase}`} />
                            </td>
                            <td> <button className="busc"  onClick={()=>eliminar(e.id)} >Eliminar</button> </td>
                            <td> <button className="busc" onClick={()=>editar(e.id)}>Editar</button> </td> 
                        </tr>
                        )}
                )}
                </tbody>
            </table>
            <div className="MovilListArticulos">
                
                {articulos.map((e)=>{
                     
                     cont = cont + 1 ;
                     return (
                        
                        <Card className="my-3" style={{ width: '18rem' }}>
                        <Card.Img variant="top"  src={`data:image/jpeg;base64,${e.imagenBase}`} />
                        <Card.Body>
                            <Card.Title className="text-dark">{e.nombre}</Card.Title>
                            
                        </Card.Body>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem><Card.Text className="text-dark" >
                            {e.descripcion}
                            </Card.Text></ListGroupItem>
                            <ListGroupItem><button className="busc"  onClick={()=>eliminar(e.id)} >Eliminar</button></ListGroupItem>
                            <ListGroupItem><button className="busc" onClick={()=>editar(e.id)}>Editar</button></ListGroupItem>
                            
                        </ListGroup>
                       
                        </Card>
                         
                         )}
                 )}
                  
            </div>
        </div>
     );
}
 
export default connect(null,{ ELejirSidebar ,SetIdEditArticulo})(GetListArticulos);