import React from "react";
import './footer.css'

export default function FooterComponent(){

    return (    
        <footer>
                        <ul className="col-12 col-md-3 list-unstyled">
                            <li className="font-weight-bold mb-2">Cruz Ali Emanuel</li>
                            <li >
                                <a href="https://www.facebook.com/aliemanuelcruz" target="_blank" className="mx-2"><i className="bi bi-facebook"></i></a>
                                <a href="https://www.linkedin.com/in/alicruzoficial/" target="_blank" className="mx-2"><i class="bi bi-linkedin"></i></a>
                            </li>
                        </ul>
                        <ul className="col-12 col-md-3 list-unstyled">
                            <li className="font-weight-bold mb-2">Diego Ortega</li>
                            <li >
                                <a href="" target="_blank" className="mx-2"><i className="bi bi-facebook"></i></a>
                                <a href="" target="_blank" className="mx-2"><i class="bi bi-linkedin"></i></a>
                            </li>
                        </ul>
                        
        </footer>
)

}