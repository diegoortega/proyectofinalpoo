import React from "react";
import './header.css'
import logo from '../../img/GRN.png'
import { Navbar,Nav,Container,NavDropdown } from "react-bootstrap";

import { useHistory } from 'react-router-dom';

export default function NavbarComponent(){
    const history = useHistory();
    const username =localStorage.getItem('USER_NAME');
    const title =<span style={{display:'flex', alignItems: 'center'}}><img 
    width="50px"
    height="50px" 
    src="https://cdn.quasar.dev/img/boy-avatar.png"/>
    <h3 style={{fontSize:'35px'}}>{username}</h3>
    </span>;
    const logOut=()=>{
    localStorage.clear();
    history.push(`/`);

    }
    return (   
        <>
            <Navbar collapseOnSelect expand="lg" style={{backgroundColor:"var(--negro)"}} variant="dark">
                <Container>
                <Navbar.Brand href="#"><img
                    alt=""
                    src={logo}
                    width="70"
                    height="70"
                    className="d-inline-block align-top"
                    /></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    </Nav>
                    <Nav >
                        <NavDropdown  title={title} id="basic-nav-dropdown">
                            <Nav.Link style={{marginTop:'5px',color:'black'}} onClick={() =>logOut()}>Logout</Nav.Link>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
                </Container>
            </Navbar>
            
            </>
        
)

}