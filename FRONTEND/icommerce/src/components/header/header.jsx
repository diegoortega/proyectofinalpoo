import React from "react";
import './header.css'
import logo from '../../img/GRN.png'
export default function HeaderComponent(){

    return (    
        <div id="HeaderComponentbx">

        <img style={{width :'90px',height :'90px',objectFit:'cover',borderRadius:'12px'}} src={logo} />
        <h2 style={{marginLeft:'1%'}}>Encuentra lo que Buscas</h2>
        </div>
)

}