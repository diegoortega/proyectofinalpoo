import url from "../urls.json";
import axios from "axios";

const getToken=()=>{
  return localStorage.getItem('USER_KEY');
}

export const fetchUserData=()=>{
  return axios({
      method:'GET',
      url:"",
      
      //con esto nos permite aquirir la informacion ya que se nesecita un token
      headers:{
          'Authorization':'Bearer '+getToken()
      }
  })
}

export const ELejirSidebar = palabra => {
  return {
    type: "ELEJIR",
    payload:palabra
  }
};  
export const getArticulos = () => {
  return async function (dispatch) {
    const { data } = await axios({
      method:'GET',
      url:`${process.env.hostUrl||'http://localhost:8080'}/articulos/getList`,
      
      //con esto nos permite aquirir la informacion ya que se nesecita un token
      headers:{
          'Authorization':'Bearer '+getToken()
      }
    })    
    dispatch({
      type: "CARGAR",
      payload: data,
    });
  };
};

export const getunArti = (id) => {
  return async function (dispatch) {
    const { data } = await axios({
      method:'GET',
      url:`${process.env.hostUrl||'http://localhost:8080'}/articulos/getList/${id}` ,
      
      //con esto nos permite aquirir la informacion ya que se nesecita un token
      headers:{
          'Authorization':'Bearer '+getToken()
      }
    }) 
    dispatch({
      type: "UNARTI",
      payload: data,
    });
  };
};

export const searchArtiXname= (name) => {
  return async function (dispatch) {
    const { data } = await  axios({
      method:'GET',
      url:`${process.env.hostUrl||'http://localhost:8080'}/articulos/getList/search/${name}`,
      
      //con esto nos permite aquirir la informacion ya que se nesecita un token
      headers:{
          'Authorization':'Bearer '+getToken()
      }
    })
    
    dispatch({
      type: "SearchXname",
      payload: data,
    });
  };
};

export const SetNombreUsuario = palabra => {
  return {
    type: "NOMBRE_USUARIO",
    payload:palabra
  }
};


export const SetIdEditArticulo = palabra => {
  return {
    type: "EDITAR_ARTICULO",
    payload:palabra
  }
};