package ar.unju.edu.fi.ventas;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.unju.edu.fi.ventas.entity.Rol;
import ar.unju.edu.fi.ventas.entity.Usuario;
import ar.unju.edu.fi.ventas.repository.RolRepository;
import ar.unju.edu.fi.ventas.repository.UsuarioRepository;

@SpringBootTest
class SistemaVentaArticulosApplicationTests {

	@Autowired	
	private UsuarioRepository repo;

	@Autowired
	private RolRepository rol;
	

	@Test
	void contextLoads() {
		Rol r = new Rol();
		Usuario s = new Usuario();
		s.setEmail("ewee");
		s.setNombre("ewewe");
		s.setPassword("123");
		s.setUsername("yamil");
		r.setNombre("ADMIN");
		//si no guardas rol va haber conflitos
		rol.save(r);
		s.setRol(r);
		repo.save(s);
	}

}
