package ar.unju.edu.fi.ventas.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ar.unju.edu.fi.ventas.DTO.MarcaDTO;
import ar.unju.edu.fi.ventas.services.IMarcaService;


@RestController
@RequestMapping(value="/marcas/")
@CrossOrigin("*")
public class MarcaController {
	private Logger logger = Logger.getLogger(MarcaController.class.getName());
	
	@Autowired
	private IMarcaService marcaService;
	
	
	@GetMapping(value="/getList")
	public List<MarcaDTO> getList(){
		return marcaService.getList();
	}
}
