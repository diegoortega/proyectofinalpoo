package ar.unju.edu.fi.ventas.repository;

import org.springframework.data.repository.CrudRepository;

import ar.unju.edu.fi.ventas.entity.Rol;


public interface RolRepository extends CrudRepository<Rol ,Long>{
	Rol findByNombre(String nombre); 
}
