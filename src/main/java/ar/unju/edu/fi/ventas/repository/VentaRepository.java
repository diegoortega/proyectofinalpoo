package ar.unju.edu.fi.ventas.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.unju.edu.fi.ventas.entity.Venta;

public interface VentaRepository extends JpaRepository<Venta, Long> {

	   
	List<Venta> findAllByFechaBetween(Date fechaDesde, Date fechaHasta);
}
