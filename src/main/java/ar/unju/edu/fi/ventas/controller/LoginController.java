package ar.unju.edu.fi.ventas.controller;

import java.security.Principal;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unju.edu.fi.ventas.DTO.ArticuloDTO;
import ar.unju.edu.fi.ventas.DTO.RolDTO;
import ar.unju.edu.fi.ventas.services.IUsuarioService;

@Controller
public class LoginController {

	@Autowired
	private IUsuarioService usuarioService;
	
	@GetMapping("/login")
	public String login2(Model model, Principal principal, RedirectAttributes flash) {
		
		if(principal != null) {
			flash.addFlashAttribute("info", "Ya has iniciado sesión");
			return "redirect:/";
		}
		
		return "login";
	}
	
	//@GetMapping(value="/login")
	//public UsuarioDTO login(Model model, Principal principal, RedirectAttributes flash){
		
	//	if(principal != null) {
	//		flash.addFlashAttribute("info", "Ya has iniciado sesión");
//			return null;
	//	}
		
	//	return usuario
	//}
}
