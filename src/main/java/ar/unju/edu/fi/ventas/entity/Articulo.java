package ar.unju.edu.fi.ventas.entity;

import java.io.Serializable;
import java.util.Base64;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "articulos")
public class Articulo implements Serializable {

	private static final long serialVersionUID = -9111772572350188934L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable= false)
	private String nombre;
	
	@ManyToOne(fetch=FetchType.EAGER)
	private Marca marca;
	
	@Column(nullable= false)
	private String descripcion;
	
	@Column(nullable= false)
	private Float precio;
	
	@Column(nullable= false)
	private Integer stock;
	
	//@Column(nullable = false)
	@Lob
	private byte[] imagen1;
	
	@Lob
	private byte[] imagen2;
	
	@Lob
	private byte[] imagen3;
	
	private byte[] imagen4;
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public byte[] getImagen1() {
		return imagen1;
	}

	public void setImagen1(byte[] imagen1) {
		this.imagen1 = imagen1;
	}

	public byte[] getImagen2() {
		return imagen2;
	}

	public void setImagen2(byte[] imagen2) {
		this.imagen2 = imagen2;
	}

	public byte[] getImagen3() {
		return imagen3;
	}

	public void setImagen3(byte[] imagen3) {
		this.imagen3 = imagen3;
	}

	public byte[] getImagen4() {
		return imagen4;
	}

	public void setImagen4(byte[] imagen4) {
		this.imagen4 = imagen4;
	}


	
	
}
