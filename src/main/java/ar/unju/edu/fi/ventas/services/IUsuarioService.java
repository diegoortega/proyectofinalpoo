package ar.unju.edu.fi.ventas.services;

import java.util.List;

import ar.unju.edu.fi.ventas.DTO.UsuarioDTO;

public interface IUsuarioService {
	public UsuarioDTO save(UsuarioDTO usuariodto);
	
	public List<UsuarioDTO> getList();
	
	public void delete(Long id);
	
	public UsuarioDTO findOne(Long id);
}
