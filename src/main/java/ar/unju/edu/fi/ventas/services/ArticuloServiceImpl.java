package ar.unju.edu.fi.ventas.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.unju.edu.fi.ventas.DTO.ArticuloDTO;
import ar.unju.edu.fi.ventas.entity.Articulo;
import ar.unju.edu.fi.ventas.entity.Marca;
import ar.unju.edu.fi.ventas.entity.Rol;
import ar.unju.edu.fi.ventas.entity.Usuario;
import ar.unju.edu.fi.ventas.repository.ArticuloRepository;
import ar.unju.edu.fi.ventas.repository.MarcaRepository;

@Service
public class ArticuloServiceImpl implements IArticuloService {

	@Autowired
	private ArticuloRepository articuloRepository;
	
	@Autowired
	private MarcaRepository marcaRepository;
	
	private ModelMapper mapper;
	
	public ArticuloServiceImpl() {
		mapper = new ModelMapper();
	}
	
	@Override
	@Transactional
	public ArticuloDTO save(ArticuloDTO articuloDTO) {
		Articulo articulo = mapToEntity(articuloDTO);
		
		Optional<Marca> marca = marcaRepository.findById((long)articuloDTO.getMarcaId());
		articulo.setMarca(marca.get());
	
		Articulo objeto = articuloRepository.save(articulo);
		return mapToDTO(objeto);
	}

	@Override
	public List<ArticuloDTO> getList() {
		
		List<ArticuloDTO> articulos = new ArrayList<>();
		
		for (Articulo articulo : articuloRepository.findAll()){
			if (articulo != null) {
				articulos.add(mapToDTO(articulo));
			}
		}
		return (articulos);
	}

	private ArticuloDTO mapToDTO(Articulo articulo) {
		return mapper.map(articulo, ArticuloDTO.class);
	}
	
	private Articulo mapToEntity(ArticuloDTO articuloDTO) {
		return mapper.map(articuloDTO, Articulo.class);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Optional<Articulo> articulo = articuloRepository.findById(id);
		if (articulo.isPresent()) {
			articuloRepository.deleteById(articulo.get().getId());
		}
		
	}

	@Override
	public ArticuloDTO findOne(Long id) {
		Optional<Articulo> articulo = articuloRepository.findById(id);
		if (articulo.isPresent()) {
			return mapToDTO(articulo.get());
		}
		return null;
	}

	@Override
	public List<ArticuloDTO> search(String filtro) {
		// TODO Auto-generated method stub
		List<ArticuloDTO> articulos = new ArrayList<>();
		
		for (Articulo articulo : articuloRepository.search(filtro)){
			if (articulo != null) {
				articulos.add(mapToDTO(articulo));
			}
		}
		return (articulos);
	}

	@Override
	public void decrementarUnidad(Long id, int cantidad) {
		Optional<Articulo> articulo = articuloRepository.findById(id);
		if (articulo.isPresent()) {
			Articulo objeto = articulo.get();
			int stock = objeto.getStock() - cantidad;
			if (stock < 0) {
				objeto.setStock(0);	
			} else {
				objeto.setStock(stock);
			}	
		}
		
	}

}
