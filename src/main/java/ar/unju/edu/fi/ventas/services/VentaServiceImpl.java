package ar.unju.edu.fi.ventas.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.unju.edu.fi.ventas.DTO.DetalleVentaDTO;
import ar.unju.edu.fi.ventas.DTO.UsuarioDTO;
import ar.unju.edu.fi.ventas.DTO.VentaDTO;
import ar.unju.edu.fi.ventas.entity.Articulo;
import ar.unju.edu.fi.ventas.entity.DetalleVenta;
import ar.unju.edu.fi.ventas.entity.Usuario;
import ar.unju.edu.fi.ventas.entity.Venta;
import ar.unju.edu.fi.ventas.repository.ArticuloRepository;
import ar.unju.edu.fi.ventas.repository.DetalleVentaRepository;
import ar.unju.edu.fi.ventas.repository.VentaRepository;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	private VentaRepository ventaRepository;
	@Autowired
	private ArticuloRepository articuloRepository;
	@Autowired
	private DetalleVentaRepository detalleRepository;
	
	private ModelMapper mapper;
	
	public VentaServiceImpl() {
		mapper = new ModelMapper();
	}
	
	@Transactional
	@Override
	public VentaDTO save(VentaDTO ventadto) {
		//Venta venta = mapToEntity(ventadto);
		Venta venta = new Venta();
		venta.setCorreo(ventadto.getCorreo());
		venta.setFecha(ventadto.getFecha());
		venta.setNombre(ventadto.getNombre());
		
		Venta objeto = ventaRepository.save(venta);
		
		ArticuloServiceImpl service = new ArticuloServiceImpl();
		
		for (DetalleVentaDTO item : ventadto.getDetalles()) {
			//DetalleVenta detalle = mapToEntityDet(item);
			DetalleVenta detalle = new DetalleVenta();
			detalle.setCantidad(1);
			detalle.setPrecio(item.getPrecio());
			detalle.setVenta(objeto);
			
			Optional<Articulo> articulo = articuloRepository.findById(item.getId());
			if (articulo.isPresent()) {
				//service.decrementarUnidad(item.getArticulo().getId(), item.getCantidad());
			}
			detalleRepository.save(detalle);
		}
		
		return mapToDTO(objeto);
	}

	@Override
	public List<VentaDTO> getList() {
		List<VentaDTO> ventas = new ArrayList<>();
		
		var listado = ventaRepository.findAll();
		
		for (Venta venta : listado){
			if (venta != null) {
				ventas.add(mapToDTO(venta));
			}
		}
		return (ventas);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Optional<Venta> venta = ventaRepository.findById(id);
		if (venta.isPresent()) {
			ventaRepository.deleteById(venta.get().getId());
		}
	}

	@Override
	public VentaDTO findOne(Long id) {
		Optional<Venta> venta = ventaRepository.findById(id);
		if (venta.isPresent()) {
			return mapToDTO(venta.get());
		}
		return null;
	}

	@Override
	public List<VentaDTO> getListByDates(Date fechaDesde, Date fechaHasta) {
		List<VentaDTO> ventas = new ArrayList<>();
		
		for (Venta venta : ventaRepository.findAllByFechaBetween(fechaDesde, fechaHasta)){
			if (venta != null) {
				ventas.add(mapToDTO(venta));
			}
		}
		return (ventas);
	}
	
	private VentaDTO mapToDTO(Venta venta) {
		return mapper.map(venta, VentaDTO.class);
	}
	
	private Venta mapToEntity(VentaDTO ventaDTO) {
		return mapper.map(ventaDTO, Venta.class);
	}
	
	
	private DetalleVenta mapToEntityDet(DetalleVentaDTO detalleVentaDTO) {
		return mapper.map(detalleVentaDTO, DetalleVenta.class);
	}

}
