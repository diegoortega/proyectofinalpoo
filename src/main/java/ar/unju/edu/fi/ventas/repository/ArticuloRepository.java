package ar.unju.edu.fi.ventas.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import ar.unju.edu.fi.ventas.entity.Articulo;


public interface ArticuloRepository extends CrudRepository<Articulo ,Long>{
	
	@Query(value="SELECT p FROM Articulo p JOIN p.marca m WHERE p.nombre LIKE %:filtro% OR m.nombre LIKE %:filtro%")   
	List<Articulo> search(@Param("filtro") String filtro);
}
