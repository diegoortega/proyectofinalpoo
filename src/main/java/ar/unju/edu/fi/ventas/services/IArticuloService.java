package ar.unju.edu.fi.ventas.services;

import java.util.List;

import ar.unju.edu.fi.ventas.DTO.ArticuloDTO;

public interface IArticuloService {
	public ArticuloDTO save(ArticuloDTO articulodto);
	
	public List<ArticuloDTO> getList();
	
	public List<ArticuloDTO> search(String filtro);
	
	public void delete(Long id);
	
	public ArticuloDTO findOne(Long id);
	
	public void decrementarUnidad(Long id, int cantidad);
}
