package ar.unju.edu.fi.ventas.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;

import ar.unju.edu.fi.ventas.DTO.UsuarioDTO;
import ar.unju.edu.fi.ventas.entity.Rol;
import ar.unju.edu.fi.ventas.entity.Usuario;
import ar.unju.edu.fi.ventas.repository.RolRepository;
import ar.unju.edu.fi.ventas.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	@Autowired 
	private RolRepository rolRepository;
	
	private ModelMapper mapper;
	
	public UsuarioServiceImpl() {
		mapper = new ModelMapper();
	}
	
	@Override
	@Transactional
	public UsuarioDTO save(UsuarioDTO usuarioDTO) {
		Usuario usuario = mapToEntity(usuarioDTO);
	
		Optional<Rol> rol = rolRepository.findById((long)usuarioDTO.getRolId());
		usuario.setRol(rol.get());
	
		Usuario objeto = usuarioRepository.save(usuario);
		return mapToDTO(objeto);
	}

	@Override
	public List<UsuarioDTO> getList() {
		
		List<UsuarioDTO> usuarios = new ArrayList<>();
		
		for (Usuario usuario : usuarioRepository.findAll()){
			if (usuario != null) {
				usuarios.add(mapToDTO(usuario));
			}
		}
		return (usuarios);
	}

	private UsuarioDTO mapToDTO(Usuario usuario) {
		return mapper.map(usuario, UsuarioDTO.class);
	}
	
	private Usuario mapToEntity(UsuarioDTO usuarioDTO) {
		return mapper.map(usuarioDTO, Usuario.class);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		if (usuario.isPresent()) {
			usuarioRepository.deleteById(usuario.get().getId());
		}
		
	}

	@Override
	public UsuarioDTO findOne(Long id) {
		Optional<Usuario> usuario = usuarioRepository.findById(id);
		if (usuario.isPresent()) {
			return mapToDTO(usuario.get());
		}
		return null;
	}
	

}
