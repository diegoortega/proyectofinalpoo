package ar.unju.edu.fi.ventas.controller;

import java.io.Console;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.unju.edu.fi.ventas.DTO.RolDTO;
import ar.unju.edu.fi.ventas.DTO.UsuarioDTO;
import ar.unju.edu.fi.ventas.helper.SendEmail;
import ar.unju.edu.fi.ventas.services.IRolService;
import ar.unju.edu.fi.ventas.services.IUsuarioService;

//@Controller
@RestController
@RequestMapping(value="/usuarios/")
@CrossOrigin("*")
public class UsuarioController {
	private Logger logger = Logger.getLogger(UsuarioController.class.getName());
	
	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private IRolService rolService;
	
	
	@RequestMapping(value="/getListUsuario", method=RequestMethod.GET)
	public String getList(Model model) {
		logger.info("Obteniendo el listado de Usuarios");
		model.addAttribute("titulo", "Listado de Usuarios");
		model.addAttribute("usuarios", usuarioService.getList());
		return "getListUsuario";		
	}
	
	@RequestMapping(value="/createUsuario")
	public String create(Map<String, Object> model) {
		logger.info("Accediendo a la pantalla de creación de Usuarios");
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		model.put("usuario", usuarioDTO);
		model.put("titulo", "Alta de Usuario");
		
		List<RolDTO> roles = rolService.getList();
		model.put("roles", roles);
		
		return "createUsuario";
	}
	
	@RequestMapping(value="/createUsuario/{id}")
	public String edit(@PathVariable(value="id") Long id, Map<String, Object> model) {
		logger.info("Accediendo a la pantalla de edición de Usuarios");
		UsuarioDTO usuarioDTO = null;
		if (id>0) {
			usuarioDTO = usuarioService.findOne(id);
		}
		
		model.put("usuario", usuarioDTO);
		model.put("titulo", "Edición de Usuario");
		List<RolDTO> roles = rolService.getList();
		model.put("roles", roles);
		return "createUsuario";
	}
	
	//metodo no ususado de creacion de usuario
	@RequestMapping(value="/createUsuario", method=RequestMethod.POST)
	public String save(UsuarioDTO usuarioDTO) {
		logger.info("Guardando Usuario");
		logger.info(usuarioDTO.getRolId().toString());
				
		usuarioService.save(usuarioDTO);
		return "redirect:getListUsuario"; 
	}
	
	
	@RequestMapping(value = "/eraseUsuario/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable(value="id") Long id) {
		logger.info("Eliminando Usuario");
		usuarioService.delete(id);
		logger.info("Usuario eliminado");
		return "redirect:/getListUsuario";
	}
	
	
	
	@GetMapping(value="/getList")
	public List<UsuarioDTO> getList(){
		return usuarioService.getList();
	}
	
	@GetMapping(value="/findBy/{id}")
	public UsuarioDTO find(@PathVariable Long id) {
		return usuarioService.findOne(id);
	}
	
    @PostMapping("/save")
	public ResponseEntity<UsuarioDTO> saveUsuario(@RequestBody UsuarioDTO usuario){
		UsuarioDTO objeto = usuarioService.save(usuario);
		return new ResponseEntity<UsuarioDTO>(objeto, HttpStatus.OK);
	}
	
	@GetMapping(value="/delete/{id}")
	public ResponseEntity<UsuarioDTO> deleteUsuario(@PathVariable Long id){
		UsuarioDTO usuarioDTO= usuarioService.findOne(id);
		if(usuarioDTO !=null) {
			usuarioService.delete(id);
		} else {
			return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<UsuarioDTO>(usuarioDTO, HttpStatus.OK);
	}
	
	
}
