package ar.unju.edu.fi.ventas.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.unju.edu.fi.ventas.entity.Usuario;
import ar.unju.edu.fi.ventas.repository.UsuarioRepository;

@Service
@Transactional
public class UsuarioService {
	@Autowired
	UsuarioRepository usuarioRepository;
	
	public Optional<Usuario> getByNombreusuario(String username){
		return usuarioRepository.findByUsername(username);
	}
	public boolean existsByNombreUsuario(String username){
		return usuarioRepository.existsByUsername(username);
	}
	public boolean existsByEmail(String email){
		return usuarioRepository.existsByEmail(email);
	}
	public void save(Usuario usuario){
		usuarioRepository.save(usuario);
	}
	
}