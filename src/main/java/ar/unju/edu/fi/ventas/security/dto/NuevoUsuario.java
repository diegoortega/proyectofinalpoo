package ar.unju.edu.fi.ventas.security.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;



public class NuevoUsuario {
	@NotBlank
	private String nombre; 		
	@NotBlank
	 private String username;
	@Email
	 private String email;
	@NotBlank
	 private String password;				
	 
	@NotBlank
	private String rol;
	 
	 
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	
	 

}
