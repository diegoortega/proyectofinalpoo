package ar.unju.edu.fi.ventas.DTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VentaDTO {
	
	private Long id;

	private String nombre;
	
	private Date fecha;
	
	private String correo;
	
	private List<DetalleVentaDTO> detalles;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<DetalleVentaDTO> getDetalles() {
		return detalles;
	}

	public void setDetalles(List<DetalleVentaDTO> detalles) {
		this.detalles = detalles;
	}

	public VentaDTO() {
		detalles = new ArrayList<DetalleVentaDTO>();
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
}
