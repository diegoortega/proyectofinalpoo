package ar.unju.edu.fi.ventas.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.unju.edu.fi.ventas.DTO.RolDTO;
import ar.unju.edu.fi.ventas.entity.Rol;
import ar.unju.edu.fi.ventas.repository.RolRepository;

@Service
public class RolServiceImpl implements IRolService {

	@Autowired
	private RolRepository rolRepository;
	
	private ModelMapper mapper;
	
	public RolServiceImpl () {
		mapper = new ModelMapper();
	}
	@Override
	@Transactional
	public void save(RolDTO roldto) {
		Rol rol = mapToEntity(roldto);
		rolRepository.save(rol);
	}

	@Override
	public List<RolDTO> getList() {
		List<RolDTO> roles = new ArrayList<>();
		
		for (Rol rol : rolRepository.findAll()){
			if (rol != null) {
				roles.add(mapToDTO(rol));
			}
		}
		return (roles);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Optional<Rol> rol = rolRepository.findById(id);
		if (rol.isPresent()) {
			rolRepository.deleteById(rol.get().getId());
		}
	}

	@Override
	public RolDTO findOne(Long id) {
		Optional<Rol> rol = rolRepository.findById(id);
		if (rol.isPresent()) {
			return mapToDTO(rol.get());
		}
		return null;
	}

	
	private RolDTO mapToDTO(Rol rol) {
		return mapper.map(rol, RolDTO.class);
	}
	
	private Rol mapToEntity(RolDTO rolDTO) {
		return mapper.map(rolDTO, Rol.class);
	}
}
