package ar.unju.edu.fi.ventas.services;

import java.util.List;

import ar.unju.edu.fi.ventas.DTO.MarcaDTO;

public interface IMarcaService {
	public void save(MarcaDTO marcadto);
	
	public List<MarcaDTO> getList();
	
	public void delete(Long id);
	
	public MarcaDTO findOne(Long id);

}
