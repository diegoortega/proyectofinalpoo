package ar.unju.edu.fi.ventas.controller;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.unju.edu.fi.ventas.DTO.ArticuloDTO;
import ar.unju.edu.fi.ventas.DTO.DetalleVentaDTO;
import ar.unju.edu.fi.ventas.DTO.VentaDTO;
import ar.unju.edu.fi.ventas.helper.SendEmail;
import ar.unju.edu.fi.ventas.services.IArticuloService;
import ar.unju.edu.fi.ventas.services.IVentaService;
import ar.unju.edu.fi.ventas.view.pdf.GeneratePdfReport;

@RestController
@RequestMapping(value="/ventas")
@CrossOrigin("*")
public class VentaController {
	private Logger logger = Logger.getLogger(ArticuloController.class.getName());
	
	@Autowired
	private IVentaService ventaService;
	
	@Autowired 
	private IArticuloService articuloService;
	
	@GetMapping(value="/getListByDates/")
	public List<VentaDTO> getListByDates(@RequestBody Date fechaDesde,@RequestBody Date fechaHasta){
		
		List<VentaDTO> listado = ventaService.getListByDates(fechaDesde, fechaHasta);
		
		return listado;
	}
	
	@GetMapping(value="/getList")
	public List<VentaDTO> getList(){
		
		List<VentaDTO> listado = ventaService.getList();
		for (VentaDTO ventaDTO : listado) {
			ventaDTO.setDetalles(null);
		}
		return listado;
	}
	
	@GetMapping(value="/findBy/{id}")
	public VentaDTO find(@PathVariable Long id) {
		return ventaService.findOne(id);
	}
	
    @PostMapping("/save")
	public ResponseEntity<VentaDTO> saveVenta(@RequestBody VentaDTO venta){
		VentaDTO objeto = ventaService.save(venta);
		
		List<ArticuloDTO> articulos = new ArrayList<ArticuloDTO>();
		for (DetalleVentaDTO item : venta.getDetalles()) {
			ArticuloDTO articulo = new ArticuloDTO();
			articulo = articuloService.findOne(item.getId());
			articulos.add(articulo);
		}
		
		SendEmail sendMail = new SendEmail(venta, articulos);
		return new ResponseEntity<VentaDTO>(objeto, HttpStatus.OK);
	}
    
    @RequestMapping(value = "/ticket/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> ticketReport(@PathVariable Long id) {

    	var ventaDTO = ventaService.findOne(id);
    	
        ByteArrayInputStream bis = GeneratePdfReport.ticketReport(ventaDTO);

        var headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=ticket.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
	
}
