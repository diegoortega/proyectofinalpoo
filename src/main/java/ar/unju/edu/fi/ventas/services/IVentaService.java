package ar.unju.edu.fi.ventas.services;

import java.util.Date;
import java.util.List;

import ar.unju.edu.fi.ventas.DTO.VentaDTO;

public interface IVentaService {
	public VentaDTO save(VentaDTO ventadto);
	
	public List<VentaDTO> getList();
	
	public void delete(Long id);
	
	public VentaDTO findOne(Long id);
	
	public List<VentaDTO> getListByDates(Date fechaDesde, Date fechaHasta);
}
