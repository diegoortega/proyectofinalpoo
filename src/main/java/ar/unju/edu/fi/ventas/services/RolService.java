package ar.unju.edu.fi.ventas.services;

import ar.unju.edu.fi.ventas.entity.Rol;
import ar.unju.edu.fi.ventas.repository.RolRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RolService {
	@Autowired
	RolRepository rolRepository;
	public Rol getByRolNombre(String nombre){
		return rolRepository.findByNombre(nombre);
		
	}

} 
