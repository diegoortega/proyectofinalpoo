package ar.unju.edu.fi.ventas.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.unju.edu.fi.ventas.entity.DetalleVenta;

public interface DetalleVentaRepository extends JpaRepository<DetalleVenta, Long> {

}
