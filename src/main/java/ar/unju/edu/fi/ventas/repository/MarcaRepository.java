package ar.unju.edu.fi.ventas.repository;

import org.springframework.data.repository.CrudRepository;

import ar.unju.edu.fi.ventas.entity.Marca;

public interface MarcaRepository extends CrudRepository<Marca, Long>{

}
