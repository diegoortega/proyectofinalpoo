package ar.unju.edu.fi.ventas.services;

import org.springframework.stereotype.Service;

import ar.unju.edu.fi.ventas.entity.Usuario;
import ar.unju.edu.fi.ventas.security.entity.UsuarioPrincipal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	UsuarioService usuarioService;
	
	@Override
	public UserDetails loadUserByUsername(String nombreUsuario) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		Usuario usuario = usuarioService.getByNombreusuario(nombreUsuario).get();
		return UsuarioPrincipal.build(usuario);
	}

}
