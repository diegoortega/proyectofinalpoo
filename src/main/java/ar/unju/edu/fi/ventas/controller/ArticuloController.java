package ar.unju.edu.fi.ventas.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.unju.edu.fi.ventas.DTO.ArticuloDTO;
import ar.unju.edu.fi.ventas.services.IArticuloService;


@RestController
@RequestMapping(value="/articulos")
@CrossOrigin()
public class ArticuloController {
	private Logger logger = Logger.getLogger(ArticuloController.class.getName());
	
	@Autowired
	private IArticuloService articuloService;
	
	@RequestMapping(value="/getListArticulo", method=RequestMethod.GET)
	public String getList(Model model) {
		logger.info("Obteniendo el listado de Articulos");
		model.addAttribute("titulo", "Listado de Articulos");
		model.addAttribute("articulos", articuloService.getList());
		return "getListArticulo";		
	}
	
	@RequestMapping(value="/createArticulo")
	public String create(Map<String, Object> model) {
		logger.info("Accediendo a la pantalla de creación de Articulo");
		ArticuloDTO articuloDTO = new ArticuloDTO();
		model.put("articulo", articuloDTO);
		model.put("titulo", "Alta de Articulo");
	
		return "createArticulo";
	}
	
	@RequestMapping(value="/createArticulo/{id}")
	public String edit(@PathVariable(value="id") Long id, Map<String, Object> model) {
		logger.info("Accediendo a la pantalla de edición de Articulos");
		ArticuloDTO articuloDTO = null;
		if (id>0) {
			articuloDTO = articuloService.findOne(id);
		}
		
		model.put("articulo", articuloDTO);
		model.put("titulo", "Edición de Articulo");
		
		return "createArticulo";
	}
	
	@RequestMapping(value="/createArticulo", method=RequestMethod.POST)
	public String save(ArticuloDTO articuloDTO) {
		logger.info("Guardando articulo");
				
		articuloService.save(articuloDTO);
		return "redirect:getListArticulo"; 
	}
	
	
	@RequestMapping(value = "/eraseArticulo/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable(value="id") Long id) {
		logger.info("Eliminando Articulo");
		articuloService.delete(id);
		logger.info("Articulo eliminado");
		return "redirect:/getListArticulo";
	}
	
	
	@RequestMapping(value="/saveArticulo", method=RequestMethod.POST)
	public String saveArticulo(@RequestBody ArticuloDTO articuloDTO) {
		articuloService.save(articuloDTO);
		return "Articulo creado!!!"; 
	}
	
	@GetMapping(value="/getList")
	public List<ArticuloDTO> getList(){
		List<ArticuloDTO> listado = articuloService.getList();
		
		for (ArticuloDTO articuloDTO : listado) {
			if(articuloDTO.getImagen1() != null) {
				Base64.Encoder encoder = Base64.getEncoder();
				articuloDTO.setImagenBase(encoder.encodeToString(articuloDTO.getImagen1()));
			}
			if(articuloDTO.getImagen2() != null) {
				Base64.Encoder encoder = Base64.getEncoder();
				articuloDTO.setImagenBase1(encoder.encodeToString(articuloDTO.getImagen2()));
			}
			if(articuloDTO.getImagen3() != null) {
				Base64.Encoder encoder = Base64.getEncoder();
				articuloDTO.setImagenBase2(encoder.encodeToString(articuloDTO.getImagen3()));
			}
		}
		return listado;
	}
	//devuelve un articulo
	@GetMapping(value="/getList/{id}")
	public ArticuloDTO getListUN(@PathVariable(value="id") Long id){
		
		ArticuloDTO articuloDTO = articuloService.findOne(id);
		
		if(articuloDTO.getImagen1() != null) {
			Base64.Encoder encoder = Base64.getEncoder();
			articuloDTO.setImagenBase(encoder.encodeToString(articuloDTO.getImagen1()));
		}
		if(articuloDTO.getImagen2() != null) {
			Base64.Encoder encoder = Base64.getEncoder();
			articuloDTO.setImagenBase1(encoder.encodeToString(articuloDTO.getImagen2()));
		}
		if(articuloDTO.getImagen3() != null) {
			Base64.Encoder encoder = Base64.getEncoder();
			articuloDTO.setImagenBase2(encoder.encodeToString(articuloDTO.getImagen3()));
		}
		
		
		return articuloDTO;
	}
	///busqueda de un articulo por marca
	@GetMapping("/getList/search/{filtro}")
	public List<ArticuloDTO> search(@PathVariable String filtro){
		
		List<ArticuloDTO> listado = articuloService.search(filtro);
		for (ArticuloDTO articuloDTO : listado) {
			if(articuloDTO.getImagen1() != null) {
				Base64.Encoder encoder = Base64.getEncoder();
				articuloDTO.setImagenBase(encoder.encodeToString(articuloDTO.getImagen1()));
			}
			if(articuloDTO.getImagen2() != null) {
				Base64.Encoder encoder = Base64.getEncoder();
				articuloDTO.setImagenBase1(encoder.encodeToString(articuloDTO.getImagen2()));
			}
			if(articuloDTO.getImagen3() != null) {
				Base64.Encoder encoder = Base64.getEncoder();
				articuloDTO.setImagenBase2(encoder.encodeToString(articuloDTO.getImagen3()));
			}
		}
		return listado;
	}
	
    @PostMapping("/save")
	public ResponseEntity<ArticuloDTO> guardarArticulo(@RequestBody ArticuloDTO articulo) throws IOException{
    	if (!articulo.getImagenPath().isEmpty()) {
    		File fi = new File(articulo.getImagenPath());
    		
    		String replacedString = articulo.getImagenPath().replaceAll("fakepath", "IMAGENARTICULO");
    		Path path = Paths.get(replacedString);
    		
    		byte[] fileContent = Files.readAllBytes(path);
    		articulo.setImagen1(fileContent);
    	}
    	
    	if (!articulo.getImagenPathAux1().isEmpty()) {
    		File fi = new File(articulo.getImagenPathAux1());
    		
    		String replacedString = articulo.getImagenPathAux1().replaceAll("fakepath", "IMAGENARTICULO");
    		Path path = Paths.get(replacedString);
    		
    		byte[] fileContent = Files.readAllBytes(path);
    		articulo.setImagen2(fileContent);
    	}
    	
    	if (!articulo.getImagenPathAux2().isEmpty()) {
    		File fi = new File(articulo.getImagenPathAux2());
    		
    		String replacedString = articulo.getImagenPathAux2().replaceAll("fakepath", "IMAGENARTICULO");
    		Path path = Paths.get(replacedString);
    		
    		byte[] fileContent = Files.readAllBytes(path);
    		articulo.setImagen3(fileContent);
    	}
		ArticuloDTO objeto = articuloService.save(articulo);
		return new ResponseEntity<ArticuloDTO>(objeto, HttpStatus.OK);
	}
}
