package ar.unju.edu.fi.ventas.controller;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ar.unju.edu.fi.ventas.DTO.RolDTO;
import ar.unju.edu.fi.ventas.services.IRolService;

//@Controller
@RestController
@RequestMapping(value="/roles/")
@CrossOrigin("*")
public class RolController {
	private Logger logger = Logger.getLogger(RolController.class.getName());
	
	@Autowired
	private IRolService rolService;

	
	@RequestMapping(value="/getListRol", method=RequestMethod.GET)
	public String getList(Model model) {
		logger.info("Obteniendo el listado de Roles");
		model.addAttribute("titulo", "Listado de Roles");
		model.addAttribute("roles", rolService.getList());
		return "getListRol";		
	}
	
	@RequestMapping(value="/createRol")
	public String create(Map<String, Object> model) {
		logger.info("Accediendo a la pantalla de creación de Roles");
		RolDTO rolDTO = new RolDTO();
		model.put("rol", rolDTO);
		model.put("titulo", "Alta de Rol");
		
		return "createRol";
	}
	
	@RequestMapping(value="/createRol/{id}")
	public String edit(@PathVariable(value="id") Long id, Map<String, Object> model) {
		logger.info("Accediendo a la pantalla de edición de Roles");
		RolDTO rolDTO = null;
		if (id>0) {
			rolDTO = rolService.findOne(id);
		}
		
		model.put("rol", rolDTO);
		model.put("titulo", "Edición de Rol");
		return "createRol";
	}
	
	@RequestMapping(value="/createRol", method=RequestMethod.POST)
	public String save(RolDTO rolDTO) {
		logger.info("Guardando Rol");
				
		rolService.save(rolDTO);
		return "redirect:getListRol"; 
	}
	
	
	@RequestMapping(value = "/eraseRol/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable(value="id") Long id) {
		logger.info("Eliminando Rol");
		rolService.delete(id);
		logger.info("Rol eliminado");
		return "redirect:/getListRol";
	}
	
	
	@RequestMapping(value="/saveRol", method=RequestMethod.POST)
	public String saveRol(@RequestBody RolDTO rolDTO) {
		rolService.save(rolDTO);
		return "Rol creado!!!"; 
	}
	
	@GetMapping(value="/getList")
	public List<RolDTO> getList(){
		return rolService.getList();
	}
}
