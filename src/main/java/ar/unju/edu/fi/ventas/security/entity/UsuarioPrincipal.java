package ar.unju.edu.fi.ventas.security.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import ar.unju.edu.fi.ventas.entity.Usuario;

public class UsuarioPrincipal implements UserDetails {
	 private String nombre;
	 private String username;
	 private String email;
	 private String password;				
	 private Collection<? extends GrantedAuthority> authorities ;
	 
	 
	 
	public UsuarioPrincipal(String nombre, String username, String email, String password,
			Collection<? extends GrantedAuthority> authorities) {
		this.nombre = nombre;
		this.username = username;
		this.email = email;
		this.password = password;
		this.authorities = authorities;
	}
	//BEAN ES EL QUE PONE LOS PRIVILEGIOS A CADA USER
	public static UsuarioPrincipal build(Usuario usuario){
		//AQUI TENGO Q MODIFICAR
		// comvirtiendo roles a autorities
		

		List<GrantedAuthority> authorities =new ArrayList<GrantedAuthority>();
		authorities.add(new SimpleGrantedAuthority(usuario.getRol().getNombre()));

		return new UsuarioPrincipal(usuario.getNombre(), usuario.getUsername(),usuario.getEmail(), usuario.getPassword(),authorities);  
	} 
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities;
	}
	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password;
	}
	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}
	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	public String getNombre() {
		return nombre;
	}
	public String getEmail() {
		return email;
	}
	
}
