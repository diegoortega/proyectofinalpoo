package ar.unju.edu.fi.ventas.services;

import java.util.List;

import ar.unju.edu.fi.ventas.DTO.RolDTO;

public interface IRolService {
	public void save(RolDTO roldto);
	
	public List<RolDTO> getList();
	
	public void delete(Long id);
	
	public RolDTO findOne(Long id);
}
