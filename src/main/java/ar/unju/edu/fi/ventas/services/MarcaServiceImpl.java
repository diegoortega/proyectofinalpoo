package ar.unju.edu.fi.ventas.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.unju.edu.fi.ventas.DTO.MarcaDTO;
import ar.unju.edu.fi.ventas.entity.Marca;
import ar.unju.edu.fi.ventas.repository.MarcaRepository;

@Service
public class MarcaServiceImpl implements IMarcaService {

	
	@Autowired
	private MarcaRepository marcaRepository;
	
	private ModelMapper mapper;
	
	public MarcaServiceImpl () {
		mapper = new ModelMapper();
	}
	
	@Override
	@Transactional
	public void save(MarcaDTO marcadto) {
		Marca marca = mapToEntity(marcadto);
		marcaRepository.save(marca);		
	}

	@Override
	public List<MarcaDTO> getList() {
		List<MarcaDTO> marcas = new ArrayList<>();
		
		for (Marca marca : marcaRepository.findAll()){
			if (marca != null) {
				marcas.add(mapToDTO(marca));
			}
		}
		return (marcas);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Optional<Marca> marca = marcaRepository.findById(id);
		if (marca.isPresent()) {
			marcaRepository.deleteById(marca.get().getId());
		}
	}

	@Override
	public MarcaDTO findOne(Long id) {
		Optional<Marca> marca = marcaRepository.findById(id);
		if (marca.isPresent()) {
			return mapToDTO(marca.get());
		}
		return null;
	}
	
	private MarcaDTO mapToDTO(Marca marca) {
		return mapper.map(marca, MarcaDTO.class);
	}
	
	private Marca mapToEntity(MarcaDTO marcaDTO) {
		return mapper.map(marcaDTO, Marca.class);
	}

}
