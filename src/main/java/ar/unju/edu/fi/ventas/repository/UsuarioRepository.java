package ar.unju.edu.fi.ventas.repository;

import org.springframework.data.repository.CrudRepository;

import ar.unju.edu.fi.ventas.entity.Usuario;
import java.util.Optional;
public interface UsuarioRepository extends CrudRepository<Usuario ,Long> {
	
	Optional<Usuario> findByUsername(String username);
	boolean existsByUsername(String username);
	boolean existsByEmail(String email);
}
