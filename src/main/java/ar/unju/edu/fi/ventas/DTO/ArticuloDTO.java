package ar.unju.edu.fi.ventas.DTO;

import java.util.Base64;

public class ArticuloDTO {

	private Long id;

	private String nombre;
	
	private String marcaNombre;
	
	private Long marcaId;
	
	private String descripcion;
	
	private Float precio;
	
	private Integer stock;
	
	private byte[] imagen1;
	
	private byte[] imagen2;
	
	private byte[] imagen3;
	
	private String imagenPath;

	private String imagenPathAux1;
	
	private String imagenPathAux2;
	
	private String imagenBase;
	
	private String imagenBase1;
	
	private String imagenBase2;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarcaNombre() {
		return marcaNombre;
	}

	public void setMarcaNombre(String marcaNombre) {
		this.marcaNombre = marcaNombre;
	}

	public Long getMarcaId() {
		return marcaId;
	}

	public void setMarcaId(Long marcaId) {
		this.marcaId = marcaId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public byte[] getImagen1() {
		return imagen1;
	}

	public void setImagen1(byte[] imagen1) {
		this.imagen1 = imagen1;
	}

	public String getImagenPath() {
		return imagenPath;
	}

	public void setImagenPath(String imagenPath) {
		this.imagenPath = imagenPath;
	}

	public String getImagenBase() {
		return imagenBase;
	}

	public void setImagenBase(String imagenBase) {
		this.imagenBase = imagenBase;
	}

	public byte[] getImagen2() {
		return imagen2;
	}

	public void setImagen2(byte[] imagen2) {
		this.imagen2 = imagen2;
	}

	public byte[] getImagen3() {
		return imagen3;
	}

	public void setImagen3(byte[] imagen3) {
		this.imagen3 = imagen3;
	}

	public String getImagenPathAux1() {
		return imagenPathAux1;
	}

	public void setImagenPathAux1(String imagenPathAux1) {
		this.imagenPathAux1 = imagenPathAux1;
	}

	public String getImagenPathAux2() {
		return imagenPathAux2;
	}

	public void setImagenPathAux2(String imagenPathAux2) {
		this.imagenPathAux2 = imagenPathAux2;
	}

	public String getImagenBase1() {
		return imagenBase1;
	}

	public void setImagenBase1(String imagenBase1) {
		this.imagenBase1 = imagenBase1;
	}

	public String getImagenBase2() {
		return imagenBase2;
	}

	public void setImagenBase2(String imagenBase2) {
		this.imagenBase2 = imagenBase2;
	}

	

}
	
